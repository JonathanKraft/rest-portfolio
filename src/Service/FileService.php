<?php

namespace App\Service;

use Symfony\Component\HttpFoundation\File\File;

class FileService {

    public function upload(File $file, string $absolutePath = ""):string {
        $path = __DIR__."/../../public/upload";

        if(!is_dir($path)) {
            mkdir($path);
        }

        $filename = uniqid() . "." . $file->guessExtension();

        $file->move($path, $filename);

        return $absolutePath . "/upload/" . $filename;
    }
}