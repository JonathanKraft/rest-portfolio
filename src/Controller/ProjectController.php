<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use App\Repository\ProjectRepository;
use App\Entity\Project;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use App\Service\FileService;
use Symfony\Component\HttpFoundation\JsonResponse;

class ProjectController extends Controller
{
    private $serializer;

    public function __construct()
    {
        $encoder = new JsonEncoder();
        $normalizer = new ObjectNormalizer();
        $normalizer->setCircularReferenceLimit(1);
        $normalizer->setCircularReferenceHandler(function ($object) {
            return $object->getId();
        });
        $this->serializer = new Serializer([$normalizer], [$encoder]);
    }

    /**
     * @Route("/project", name="all_projects", methods={"GET"})
     */
    public function all(ProjectRepository $repo)
    {
        $list = $repo->findAll();

        $data = $this->serializer->normalize($list, null, ['attributes' => ['id', 'link', 'title', 'description', 'imageLink', 'gitlab']]);

        $response = new JsonResponse($this->serializer->serialize($data, "json"));
        return $response;
    }

    /**
     * @Route("/project", name="new_project", methods={"POST"})
     */
    public function add(Request $request, FileService $fileService)
    {
        $image = $request->files->get("image");
        $absoluteUrl = $request->getScheme() . '://' . $request->getHttpHost() . $request->getBasePath();
        $imageUrl = $fileService->upload($image, $absoluteUrl);

        $project = new Project();
        $project->setDescription($request->get("description"));
        $project->setTitle($request->get("title"));
        $project->setGitlab($request->get("gitlab"));
        $project->setLink($request->get("link"));
        $project->setImageLink($imageUrl);

        $manager = $this->getDoctrine()->getManager();
        $manager->persist($project);
        $manager->flush();

        $json = $this->serializer->serialize($project, "json");

        return JsonResponse::fromJsonString($json);
    }

    /**
     * @Route("/project/{project}", name="delete_project", methods={"DELETE"})
     */
    public function del(Project $project)
    {
        $manager = $this->getDoctrine()->getManager();

        $manager->remove($project);
        $manager->flush();

        return new Response("OK", 204);
    }

    // /**
    //  * @Route("/project/{project}", name="updatde_project", methods={"PUT"})
    //  */
    // public function upd(Request $request, Project $project)
    // {
    //     $manager = $this->getDoctrine()->getManager();

    //     $content = $request->getContent();
    //     $update = $this->serializer->deserialize($content, Project::class, "json");

    //     $project->setLink($update->getLink());
    //     $project->setTitle($update->getTitle());
    //     $project->setDescription($update->getDescription());
    //     $project->setImageLink($update->getImageLink());
    //     $project->setGitlab($update->getGitlab());

    //     $manager->persist($project);
    //     $manager->flush();

    //     $data = $this->serializer->normalize($project, null, ['attributes' => ['id', 'link', 'title', 'description', 'imageLink', 'gitlab']]);        

    //     $response = new Response($this->serializer->serialize($project, "json"));
    //     return $response;
    // }

}

